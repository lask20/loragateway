var server = require('http').createServer();
var express = require('express');
var app = express();
var socket = require('socket.io-client')('http://loragateway.ddns.net:4000',{ //http://loragateway.ddns.net:4000
	 path: '/socket/gateway',
  transportOptions: {
    polling: {
      extraHeaders: {
        'x-clientid': 1421455850
      }
    }
  }
});
var SX127x = require('sx127x');

var parsePacket = require('./parsePacket');
var processThing = require('./processThing');

var sx127x = new SX127x({
	frequency: 433E6,
	spreadingFactor: 7,
	codingRate:4/8,
	signalBandwidth:125E3

});

var things = new processThing(sx127x);
things.socket(socket);




app.set('view engine', 'ejs');
app.use(express.static('public'));

app.get('/', function(req, res) {
	console.log(things.getThing());
	res.render('pages/index',{things:things.getThing(),server:things.getServerStatus()});
});







server.on('request', app);


server.listen(3000, function() {

	console.log(`http/ws server listening on 3000`);
});