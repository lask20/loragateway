var thing = require('./thing');
var parsePacket = require('./parsePacket');

var MAX_DATA  = 16;
var PREFIX  = 0x11;

var JOIN_GATEWAY  = 0;
var GATEWAY_ACCEPTED  = 1;
var CHECK_ALIVE  = 2;
var REPLY_ALIVE  = 3;
var SLEEP_TIME  = 4;
var REPLY_SLEEP_TIME  = 5;
var SEND_MESSAGE  = 6;
var REPLY_SEND_MESSAGE  = 7;
var REJECT  = 8;
var AUTHFAIL = 9;

class processThing{ 
	constructor(sx127x){
		this._sx127x = sx127x;
		this._thing = {};
		this._connectionServer = false;

		this._initLora();
	}

	socket(socket) {
		this._socket = socket;
		this._socket.on('connect', this._socketConnect.bind(this));

		// this._socket.on('event', this._socketData.bind(this));
		this._socket.on('data', this._socketData.bind(this));
		this._socket.on('disconnect', this._socketDisconnect.bind(this));
	}

	_socketConnect() {
		console.log("conected");
		this._connectionServer = true;
		for (var i = 0, len = this._thing.length; i < len; i++) {
			this._socket.emit('join', this._thing[i].source);
		}
		
	}

	_socketDisconnect() {
		this._connectionServer = false;
	}

	_socketData(data) {
		
		if (data.destination in this._thing) {
			this._sendMessage(data);
		}
		
	}

	getServerStatus() {
		return this._connectionServer;
	}


	getThing() {
		return this._thing;
	}

	_initLora() {
		this._sx127x.open((function(err) {
			console.log('open', err ? err : 'success');

			if (err) {throw err;}

			this._sx127x.on('data', this._processMessage.bind(this));

			this._sx127x.receive(function(err) {if (err) {throw err;}});

		}).bind(this));
	}

	_processMessage(data,rssi) {
		var packet = new parsePacket();
		packet.setRSSI(rssi);

		if (packet.parse(data)) {
			console.log(packet);
			this._join(packet);	
			if (packet.type == SEND_MESSAGE){		
				if (this._hasJoin(packet)) {
					this._thing[packet.source].packet(packet);
				}
				else {
					this._sendReject(packet);
				}
			}
		}

	}
	_processMessageType(packet) {

		switch(packet.type) {
			case JOIN_GATEWAY:	
			break;
			case GATEWAY_ACCEPTED:
			break;
			case CHECK_ALIVE:
			break;
			case REPLY_ALIVE:
			break;
			case SLEEP_TIME:
			break;
			case REPLY_SLEEP_TIME:
			break;
			case SEND_MESSAGE:
			this._isHasThing(packet);
			break;
			case REPLY_SEND_MESSAGE:
			break;
		}

	}

	_hasJoin(packet) {
		return this._hasThing(packet.source);
	}

	_join(packet) {
		if (packet.type == JOIN_GATEWAY) {
			if (this._hasJoin(packet) ) {
				delete this._thing[packet.source];
			}
			this._socket.emit('join', packet.source, (function (data) { 
				console.log(data);
				if (data.accepted) {
					this._newThing(packet);
				}
				else {
					this._sendAuthFail(packet);
				}
			}).bind(this));
		}
	}

	_hasThing(id) {
		if (id in this._thing){
			return true;
		}
		else {
			return false;
		}
	}

	_isHasThing(packet) {
		// var thingObj = this._getThing(packet.source);
		if (packet.source in this._thing){
			this._thing[packet.source].packet(packet);
			// thingObj.packet(packet);
		}
		else {
			this._newThing(packet);
		}
		
	}

	_newThing(packet) {
		console.log("crate new thing");
		this._thing[packet.source] = new thing(packet.source);
		this._thing[packet.source].send(this._send.bind(this));
		this._thing[packet.source].socket(this._socket);
		this._thing[packet.source].replyJoin(packet);
		
	}
	
	_sendMessage(packet) {
		var replyPacket = new parsePacket();
		replyPacket.source = packet.source;
		replyPacket.destination = packet.destination;
		replyPacket.type = SEND_MESSAGE;
		replyPacket.data = packet.data;
		replyPacket.seq = 0;

		console.log(replyPacket);

		this._send(replyPacket);
	}

	_sendReject(packet) {
		var replyPacket = new parsePacket();
		replyPacket.source = packet.destination;
		replyPacket.destination = packet.source;
		replyPacket.type = REJECT;
		replyPacket.seq = packet.seq;

		this._send(replyPacket);
	}

	_sendAuthFail(packet) {
		var replyPacket = new parsePacket();
		replyPacket.source = packet.destination;
		replyPacket.destination = packet.source;
		replyPacket.type = AUTHFAIL;
		replyPacket.seq = packet.seq;

		this._send(replyPacket);
	}


	_send(packet) {
		// console.log(packet);
		this._sx127x.write(packet.toBuffer(),(function(err) {
			if (err) {throw err;}
			this._sx127x.receive(function(err) {if (err) {throw err;}});
		}).bind(this));
	}

}

module.exports = processThing;