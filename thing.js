var parsePacket = require('./parsePacket');

class thing{ 
	constructor(id){
		this.id = id;
		this.rx = 0;
		this.tx = 0;
		this.lossPacket = 0;
		this.lastActive;
		this.connectAt = Date.now();
		this.lastSeq = 0;
		this.lastRssi;
	}

	socket(socket) {
		this._socket = socket;
	}

	replyJoin(packet) {
		this._sendReply(packet);
	}

	send(callback) {
		this._send = callback;
	}

	packet(packet) {
		if (packet.source == this.id) {
			this.lastRssi = packet.rssi;
			this.lastActive = Date.now();
			this._sendReply(packet);
			this._checkSeq(packet.seq);
			this.rx++;
			this._forwarder(packet);

		}
	}


	_forwarder(packet) {
		this._socket.emit("data",packet);
	}

	onMessage(data) {

	}

	_processMessageType(packet) {

		switch(packet.type) {
			case JOIN_GATEWAY:	
			break;
			case GATEWAY_ACCEPTED:
			break;
			case CHECK_ALIVE:
			break;
			case REPLY_ALIVE:
			break;
			case SLEEP_TIME:
			break;
			case REPLY_SLEEP_TIME:
			break;
			case SEND_MESSAGE:
				this._isHasThing(packet);
			break;
			case REPLY_SEND_MESSAGE:
			break;
		}

	}

	_sendReply(packet) {
		var replyPacket = new parsePacket();
  		replyPacket.source = packet.destination;
  		replyPacket.destination = packet.source;
  		replyPacket.type = packet.type + 1;
  		replyPacket.seq = packet.seq;

  		// console.log(replyPacket);
  		this._send(replyPacket);
	}

	_checkSeq(seq) {
		if (seq > this.lastSeq) {
			
			var loss = seq-this.lastSeq;
			if (loss > 1) {
				this.lossPacket += loss;
			}
			this.lastSeq = seq;
		}
	}


}

module.exports = thing;